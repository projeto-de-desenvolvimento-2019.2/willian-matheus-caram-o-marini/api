# Control Game
## Aplicação Mobile para gestão de atividades esportivas na modalidade do Basquete.

Projeto desenvolvido para a cadeira de Projeto de Desenvolvimento da Faculdade Senac - Pelotas.


## Requisitos Minimos para instalar o projeto

*  Flutter SDK instalado na maquida - Tutorial [aqui](https://flutter.dev/docs/get-started/install)
*  Dart SDK instalado na maquida - Tutorial [aqui](https://dart.dev/get-dart#install)
*  [Android Studio](https://developer.android.com/studio/?gclid=CjwKCAiArJjvBRACEiwA-Wiqq36t64ga8jSLt9zMKwsJIrvkipOk2A44BOTvmk7iCN-iGXQdAwn4RxoCqbsQAvD_BwE)/[IntelliJ IDEA](https://www.jetbrains.com/idea/download/) instalados preferencialmente

## Passos
Faça clone do repositorio na sua máquina 

`git clone https://gitlab.com/projeto-de-desenvolvimento-2019.2/willian-matheus-caram-o-marini/mobile.git`

Abra o repositorio em no Android Studio ou IntelliJ IDEA

Configure um emulador ou conecte um dispositivo compativel com o seu sistema operacional

Selecione a opção "Run" ou tecle `Shift + F10`

O Projeto pode ser rodado pelo terminal também com o comando abaixo:

`flutter run`
