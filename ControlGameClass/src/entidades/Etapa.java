package entidades;

import java.util.Date;
import java.util.List;

public class Etapa {
    int id;
    String nome;
    Date criação;
    Date dataEtapa;
    List<Jogo> jogos;
    List<Time> times;

    int tipoDeEtapa;
    /**
     * 1 - Torneio sem anotação estatistica dos atletas por jogo
     * 2 - Torneio com anotação estatistica dos atletas por jogo
     * 3 - ???
     * */

    boolean useShot;
    boolean useRebote;
    boolean useAssistencia;
    boolean useBloqueio;
    boolean useRoubo;
    boolean useErro;
    boolean[] useStats  = {useShot, useRebote, useAssistencia, useBloqueio, useRoubo, useErro};
}
